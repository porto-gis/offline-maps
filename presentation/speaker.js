import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

const PersonalImg = styled((props) => <img {...props} />)`
  width: 128px;
  border-radius: 64px;
`;

const CompanyImg = styled((props) => <img {...props} />)`
  width: 128px;
`;

const Speaker = ({ className, name, image, company, companyLogo }) =>  (
  <div className={className}>
    <PersonalImg src={image} />
    <h3>{name}</h3>
    {companyLogo ? <CompanyImg src={companyLogo} /> : <h2>{company}</h2>}
  </div>
);

Speaker.propTypes = {
  name: PropTypes.string.isRequired,
  image: PropTypes.string,
  company: PropTypes.string,
  companyLogo: PropTypes.string,
};

Speaker.defaultProps = {
  image: null,
  company: null,
  companyLogo: null,
};

const styledSpeaker = styled(Speaker)`
  display: inline-block;
`;

const Speakers = ({ className, children }) => (
  <div className={className}>
    { children }
  </div>
);

const styledSpeakers = styled(Speakers)`
  display: flex;
  justify-content: space-around;
  margin-top: 3rem;
`;

export default Speaker;
export {
  styledSpeaker as Speaker,
  styledSpeakers as Speakers
}